Finers 广告系统 - 广告投放
===============

当前最新版本： 1.0.0（发布日期：2020-06-21）


[![AUR](https://img.shields.io/badge/license-Apache%20License%202.0-blue.svg)]()
[![](https://img.shields.io/badge/Author-FINERS团队-orange.svg)]()
[![](https://img.shields.io/badge/version-1.0.0-brightgreen.svg)]()


介绍
-----------------------------------
Finers广告系统【完全开源】是一款网页广告管理及投放的产品，可作为公共平台为一个或多个网站提供广告服务。
系统分为广告管理和广告投放两部分，本部分为其中的广告投放。
- 按设定的广告位类型、广告内容和投放策略，向网页投放广告，并记录和统计广告呈现与点击日志
- 只需简单的两行代码，即可在网页中嵌入广告


环境搭建和运行
-----------------------------------
1、开发环境
- 语言：Node.JS
- IDE：Visual Studio Code（推荐）
- 数据库：MySQL5.7+ （本地开启事件）

2、数据库初始化：
- 如果已经使用了“ad-admin\jeecg-boot\db\jeecgboot&ads_mysql5.7.sql”初始，则忽略本步骤。
- 数据库初始脚本：ad-put\db\ad-put-mysql5.7.sql

3、安装node.js

4、切换到ads-put文件夹下

5、修改配置
- 配置文件：config/.env
```
#服务器的启动名字和端口
export HOST=0.0.0.0
export PORT=8090

#数据库配置
export DB_DIALECT=mysql
export DB_HOST=127.0.0.1
export DB_PORT=3306
export DB_USERNAME=root
export DB_PASSWORD=root
export DB_NAME=ads

#上传文件目录
export PATH_UPLOAD_ADFILE=D:/opt/upFiles
```
PATH_UPLOAD_ADFILE：设置为广告管理（后台）的上传文件目录。

6、安装依赖
```
npm install
```

7、启动投递服务
```
npm run start
```

网页嵌入广告
-----------------------------------
- script方式
```
  <script class="CLASS42bc4e2f_b826_11e9_9ed0_18dbf2568723" src="http://127.0.0.1:8090/js/ad.js"></script>
  ...
  <script>ad('demo-embed-image-rotate', getCurrentScript());</script>
```
demo-embed-image-rotate：换成实际广告位的ID（广告位登记后生成）。

- iframe方式：不支持漂浮与弹窗类型
```
  <script class="CLASS42bc4e2f_b826_11e9_9ed0_18dbf2568723" src="http://127.0.0.1:8090/js/ad.js"></script>
  ...
  <iframe width="410" height="410" onload="ad('demo-embed-image-rotate', this);" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" ></iframe>
```
demo-embed-image-rotate：换成实际广告位的ID（广告位登记后生成）。

- 参考样例：demo\demo.html
