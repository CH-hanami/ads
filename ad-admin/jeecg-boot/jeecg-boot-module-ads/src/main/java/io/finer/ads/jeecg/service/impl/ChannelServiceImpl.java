package io.finer.ads.jeecg.service.impl;

import io.finer.ads.jeecg.entity.Channel;
import io.finer.ads.jeecg.mapper.ChannelMapper;
import io.finer.ads.jeecg.service.IChannelService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 广告频道
 * @Author: jeecg-boot
 * @Date:   2020-06-16
 * @Version: V1.0
 */
@Service
public class ChannelServiceImpl extends ServiceImpl<ChannelMapper, Channel> implements IChannelService {

}
